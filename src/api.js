const BASE_URL = 'https://d22d8757-af91-4af0-aa1a-457955f1f800.mock.pstmn.io';


const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
const randomNumber = (min = 0, max = 1) =>
  Math.floor(Math.random() * (max - min + 1)) + min;
const simulateNetworkLatency = (min = 30, max = 1500) =>
  delay(randomNumber(min, max));

async function callApi(endpoint, options = {}) {
  await simulateNetworkLatency();

  options.headers = {
    'Content-Type': 'application/json',
    Accept: 'accept-encoding: gzip, deflate',
  };

  //const url = BASE_URL + endpoint;
  const url =  endpoint;

  const response = await fetch(url, options);
  const data = await response.json();

  return data;
}

// "/api/User/Register": "/User",
// "/api/complexDomain/GetAll": "/GetAllComplex",
// "/api/WaterAttributes/GetAll": "/GetAllWater",
// "/api/simpleDomain/GetAll": "/GetAllSimple

const api = {
  badges: {
    listSimple() {
      return callApi('https://4d575573-d34c-48eb-afa7-3f4957b4b2b8.mock.pstmn.io/api/simpleDomain/GetAll', {
        method: 'POST'
      });
    },
    listComplex() {
      return callApi('https://d22d8757-af91-4af0-aa1a-457955f1f800.mock.pstmn.io/api/ComplexDomain/GetAll', {
        method: 'POST',
        body: JSON.stringify({
          "User":"usuario",
          "Data":null
        }),

        
      });
    },
    listagua() {
      return callApi('https://11a53a0a-3236-417c-9eaa-a4a620767b0c.mock.pstmn.io/api/WaterAttributes/GetAll', {
        method: 'POST'
      });
    },
    listTipo() {
      return callApi('https://443369b4-453f-4497-aeb6-f0402b7682bd.mock.pstmn.io/api/EntityType/GetAll', {
        method: 'POST'
      });
    },
    listnavegable() {
      return callApi('https://dc3b48e1-2a7a-4480-8292-0c4bac382442.mock.pstmn.io/api/NavigableRiver/GetAll', {
        method: 'POST'
      });
    },
    listpotencial() {
      return callApi('https://37b7193b-9519-4b99-b69f-ce61e489ee1a.mock.pstmn.io/api/Potential/GetAll', {
        method: 'POST'
      });
    },
    listentidad() {
      return callApi('https://948bc06f-d41f-4bd4-89df-91c3dcc99ca1.mock.pstmn.io/api/entity/GetAll', {
        method: 'POST'
      });
    },
    saveSimple(badge) {
      return callApi('/api/simpleDomain/Update', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    updateSimple(badge) {
      return callApi('/api/simpleDomain/Update', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    saveComplex(badge) {
      return callApi('/api/complexDomain/Save', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    updateComplex(badge) {
      return callApi('/api/complexDomain/Update', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    saveAgua(badge) {
      return callApi('/api/WaterAttributes/Save', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    updateAgua(badge) {
      return callApi('/api/WaterAttributes/Update', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    saveTipo(badge) {
      return callApi('/api/entityType/Save', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    updateTipo(badge) {
      return callApi('/api/entityType/Update', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    saveNavegable(badge) {
      return callApi('/api/NavigableRiver/Save', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    updateNavegable(badge) {
      return callApi('/api/NavigableRiver/Update', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    saveEntidad(badge) {
      return callApi('/api/entity/Save', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    updateEntidad(badge) {
      return callApi('/api/entity/Update', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    savePotencial(badge) {
      return callApi('/api/Potential/Save', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    updatePotencial(badge) {
      return callApi('/api/Potential/Update', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },

    login(badge) {
      return callApi('https://478d4a04-151a-4c1f-a660-4f9d4d186408.mock.pstmn.io/api/User/Login', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },

    register(badge) {
      return callApi('https://478d4a04-151a-4c1f-a660-4f9d4d186408.mock.pstmn.io/api/User/Register', {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },

    create(badge) {
      return callApi(`/badges`, {
        method: 'POST',
        body: JSON.stringify(badge),
      });
    },
    read(badgeId) {
      return callApi(`/badges/${badgeId}`);
    },
    update(badgeId, updates) {
      return callApi(`/badges/${badgeId}`, {
        method: 'PUT',
        body: JSON.stringify(updates),
      });
    },
    // Lo hubiera llamado `delete`, pero `delete` es un keyword en JavaScript asi que no es buena idea :P
    remove(badgeId) {
      return callApi(`/badges/${badgeId}`, {
        method: 'DELETE',
      });
    },
  },
};

export default api;
