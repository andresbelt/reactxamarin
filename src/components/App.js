import React from 'react';

import { BrowserRouter, Switch, Route } from 'react-router-dom';

import BadgeNew from '../pages/BadgeNew';
import Badges from '../pages/Badge';


function App() {
  return (
    <BrowserRouter>
        <Switch>
          <Route exact path="/" component={BadgeNew} />
          <Route exact path="/badges/:domcom" component={Badges} />
        </Switch>
    </BrowserRouter>
  );
}

export default App;