import React from 'react';


import { Modal } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { ButtonToolbar } from 'react-bootstrap';
import { ButtonGroup } from 'react-bootstrap';


export class ModalRecordar extends React.Component {
    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Body>
                     <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Correo de electronico</Form.Label>
                            <Form.Control type="email" placeholder="Ingrese correo de electronico" />

                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Contraseña</Form.Label>
                            <Form.Control type="password" placeholder="Ingrese contraseña" />
                        </Form.Group>

                        <ButtonToolbar >

                            <ButtonGroup className="mr-2" aria-label="First group">
                                <Button variant="light">Recordar</Button>
                            </ButtonGroup>

                            <ButtonGroup className="mr-2" aria-label="Second group">
                            <Button variant="light" onClick={this.props.onHide}>Cancelar</Button>

                            </ButtonGroup>

                        </ButtonToolbar>
                   
                    </Form>
                </Modal.Body>


            </Modal>
        );
    }
}


export default ModalRecordar;