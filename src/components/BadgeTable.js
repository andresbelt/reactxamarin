import React from 'react';
import { Table } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import api from '../api';

class BadgeTable extends React.Component {

    fetch = async (param) => {
        this.setState({ loading: true, error: null });
        var data = null;
        switch (this.props.param) {

            case 'dominio':

                try {
                    if (param === 1) {
                        data = await api.badges.saveComplex();
                    } else {
                        data = await api.badges.updateComplex();
                    }
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;
            case 'dominioSimple':

                try {
                    if (param === 1) {
                        data = await api.badges.saveSimple();
                    } else {
                        data = await api.badges.updateSimple();
                    }
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }

                break;
            case 'tipo':

                try {
                    if (param === 1) {
                        data = await api.badges.saveTipo();
                    } else {
                        data = await api.badges.updateTipo();
                    }
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;

            case 'navegable':

                try {
                    if (param === 1) {
                        data = await api.badges.saveNavegable();
                    } else {
                        data = await api.badges.updateNavegable();
                    }
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;

            case 'potencial':

                try {
                    if (param === 1) {
                        data = await api.badges.savePotencial();
                    } else {
                        data = await api.badges.updatePotencial();
                    }
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;

            case 'agua':

                try {
                    if (param === 1) {
                        data = await api.badges.saveAgua();
                    } else {
                        data = await api.badges.updateAgua();
                    }
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;


            case 'entidad':

                try {
                    if (param === 1) {
                        data = await api.badges.saveEntidad();
                    } else {
                        data = await api.badges.updateEntidad();
                    }
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;

                default:
                break;

        }


    };


    render() {

        return (
            <div style={{ paddingTop: '1rem', paddingLeft: '1rem', paddingRight: '1rem' }} >
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Estado</th>
                            <th>Acción</th>
                        </tr>
                    </thead>

                    <tbody>
                        {this.props.badges.Data.map((data, i) => {
                            return (
                                <tr key={i}>
                                    <td>{data.Code}</td>
                                    <td>{data.Characteristic}</td>
                                    <td>
                                        {data.IsEnable ? <a >Activo</a> : <a >Desactivo</a>}
                                    </td>
                                    <td>
                                        <Button variant="outline-secondary" onClick={() => this.fetch(1)}>Editar</Button>
                                        <Button variant="outline-secondary" onClick={() => this.fetch(0)}>Remover</Button></td>

                                </tr>
                            )
                        })}
                    </tbody>
                </Table>
            </div>
        );
    }
}



export default BadgeTable;
