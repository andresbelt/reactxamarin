import React from 'react';


import { Modal } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { ButtonToolbar } from 'react-bootstrap';
import { ButtonGroup } from 'react-bootstrap';
import api from '../api';


export class ModalRegister extends React.Component {

    constructor(...args) {
        super(...args);

        this.state = { modalShow: false ,
            form: {
                name: '',
                user: '',
                password: '',
                passconf: '',
                email: '',
              },
            };
    }


    handleChange = (e) => {
        this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
            
        })

       console.log("EMail: " + JSON.stringify(this.state.form));
    }

    onSubmit = (e,props) => {
        e.preventDefault();
        const form = {
         email: this.state.email,
         password: this.state.password
        }
     
       console.log("EMail: " + this.state.form);

       this.fetchData(form,props);
      
    }


    fetchData = async (form,props) => {
        this.setState({ loading: true, error: null });
    
        try {
          const data = await api.badges.register(form);
          this.setState({ loading: false, data: data });
          props.onHide()
          console.log(data)

        } catch (error) {
          this.setState({ loading: false, error: error });
          console.log(error)
        }
      };

    

    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Nombre completo</Form.Label>
                            <Form.Control onChange={e => this.handleChange(e)} value={this.state.form.name || ''} name="name" placeholder="Ingrese nombre completo" />

                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Nombre de usuario</Form.Label>
                            <Form.Control onChange={e => this.handleChange(e)} value={this.state.form.user || ''}   name="user" placeholder="Ingrese nombre de usuario" />
                        </Form.Group>

                          <Form.Group controlId="formBasicPassword">
                            <Form.Label>Contraseña</Form.Label>
                            <Form.Control onChange={e => this.handleChange(e)} value={this.state.form.password || ''}  type="password" name="password"  placeholder="Ingrese contraseña" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Confirmacion de contraseña</Form.Label>
                            <Form.Control onChange={e => this.handleChange(e)} type="password" name="passconf" value={this.state.form.passconf || ''}  placeholder="Ingrese la contraseña nuevamente" />
                        </Form.Group>

                     <Form.Group controlId="formBasicPassword">
                            <Form.Label>Correo de electronico</Form.Label>
                            <Form.Control onChange={e => this.handleChange(e)} type="Correo de electronico" value={this.state.form.email || ''} name="email" placeholder="Ingrese correo de electronico" />
                        </Form.Group>

                        <ButtonToolbar >

                            <ButtonGroup className="mr-2" aria-label="First group">
                                <Button variant="light" onClick={(e) => this.onSubmit(e,this.props)} type="submit">Registrar</Button>
                            </ButtonGroup>

                            <ButtonGroup className="mr-2" aria-label="Second group">
                            <Button variant="light" onClick={this.props.onHide}>Cancelar</Button>

                            </ButtonGroup>

                        </ButtonToolbar>
                
                    </Form>
                </Modal.Body>


            </Modal>
        );
    }
}


export default ModalRegister;