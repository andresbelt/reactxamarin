import React from 'react';


import { Modal } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { ButtonToolbar } from 'react-bootstrap';
import { ButtonGroup } from 'react-bootstrap';
import ModalRegister from '../components/ModalRegister';
import ModalRecordar from '../components/ModalRecordar';
import api from '../api';


export class ModalCenter extends React.Component {
    constructor(...args) {
        super(...args);

        this.state = {
            showModal: true, modalShowRecordar: false, modalShowRegister: false,
                email: '',
                password: ''
       
        };
    }


    close() {
        this.setState({ showModal: false });
    }

    fetchData = async (form, props) => {
        this.setState({ loading: true, error: null });

        try {
            const data = await api.badges.login(form);
            this.setState({ loading: false, data: data });
            props.onHide()
        } catch (error) {
            this.setState({ loading: false, error: error });

        }
    };


    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    onSubmit = (e, props) => {
        e.preventDefault();
        const form = {
            email: this.state.email,
            password: this.state.password
        }
      
        console.log("EMail: " + this.state.email);
        console.log("EMail: " + this.state.password);
        this.fetchData(form, props);

    }


    handleLogin() {
        console.log("EMail: " + this.state.email);
        console.log("Password: " + this.state.password);
    }

    registrarse(props) {
        //props.onHide();
        this.setState({ modalShowRegister: true });

    }


    render() {
        let modalCloseRecordar = () => this.setState({ modalShowRecordar: false });
        let modalCloseRegister = () => this.setState({ modalShowRegister: false });
        return (




            <Modal
                size="lg"
                {...this.props}
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >

                <ModalRegister
                    show={this.state.modalShowRegister}
                    onHide={modalCloseRegister}
                />



                <ModalRecordar
                    show={this.state.modalShowRecordar}
                    onHide={modalCloseRecordar}
                />


                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Correo de electronico</Form.Label>
                            <Form.Control value={this.state.email || ''}
                                onChange={e => this.handleChange(e)} name="email" type="email" placeholder="Ingrese correo de electronico" />

                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Contraseña</Form.Label>
                            <Form.Control type="password" name="password" placeholder="Contraseña" value={this.state.password || ''}
                                onChange={e => this.handleChange(e)} />
                        </Form.Group>

                        <ButtonToolbar >

                            <ButtonGroup className="mr-2" aria-label="First group">
                                <Button variant="light" onClick={(e) => this.onSubmit(e, this.props)} type="submit" >Login</Button>
                            </ButtonGroup>

                            <ButtonGroup className="mr-2" aria-label="Second group">
                                <Button variant="light" onClick={this.props.onHide}>Cancelar</Button>

                            </ButtonGroup>

                        </ButtonToolbar>
                        <Form.Text >
                            <a onClick={() => { this.registrarse(this.props) }} >Registrarse</a>

                        </Form.Text>

                        <Form.Text className="text-muted">
                            <a onClick={() => this.setState({ modalShowRecordar: true })

                            } >Olvidé mi contraseña.</a>

                        </Form.Text>
                    </Form> 
                </Modal.Body>


            </Modal>
        );
    }
}


export default ModalCenter;