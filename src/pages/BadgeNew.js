import React from 'react';

import './styles/BadgeNew.css';
import { Navbar } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';

import { Container } from 'react-bootstrap';
import { Row } from 'react-bootstrap';

import { Card } from 'react-bootstrap';
import { CardColumns } from 'react-bootstrap';

import ModalCenter from '../components/ModalCenter';

import Atributos from '../images/Atributos.jpg';
import Dominio_simple from '../images/Dominio_simple.jpg';
import Dominio from '../images/Dominio.jpg';
import Entidad from '../images/Entidad.jpg';
import logo from '../images/Logo.png';
import Potencial from '../images/Potencial.jpg';
import Red_navegable from '../images/Red_navegable.jpg';
import Tipo_entidad from '../images/Tipo_entidad.jpg';

import usuario from '../images/Usuario.png';


class BadgeNew extends React.Component {

    constructor(...args) {
        super(...args);

        this.state = { modalShow: false };
    }


    render() {
        let modalClose = () => this.setState({ modalShow: false });

        return (

            <div>

                {/* <Button
                    variant="primary"
                    onClick={() => this.setState({ modalShow: true })}>
                    Launch vertically centered modal
                </Button> */}

                <ModalCenter
                    show={this.state.modalShow}
                    onHide={modalClose}
                />

                <Navbar bg="light" expand="lg">

                    <Navbar.Brand href="">
                        <img className="mama" src={logo} alt="Logo" />
                        RIO VERDE</Navbar.Brand>
                    <Nav className="mr-auto">

                        <Nav.Link href="" className="mr-sm-2">Inicio</Nav.Link>
                        <Nav.Link href="">Administración</Nav.Link>
                        <Nav.Link href="">Aprender</Nav.Link>

                    </Nav>


                    <Navbar.Text >
                        <Container >

                            {false ? <Row  >
                                <a >Mark</a>
                            </Row> 
                            
                            : null}

                        </Container>

                    </Navbar.Text>


                    <Nav.Link eventKey={2} >
                        <img className="mama" onClick={() => this.setState({ modalShow: true })} src={usuario} alt="Logo" />
                    </Nav.Link>
                </Navbar>


                <div className="Badge_New__hero">

                    <CardColumns className="ccolumn" >
                        <Card >
                            <Card.Img variant="bottom" src={Dominio} />
                            <Card.Body>
                                <Card.Link href="/badges/dominio">DOMINIO</Card.Link>
                            </Card.Body>

                        </Card>
                        <Card >
                            <Card.Img variant="bottom" src={Dominio_simple} />
                            <Card.Body>
                                <Card.Link href="/badges/dominioSimple">DOMINIO SIMPLE</Card.Link>
                            </Card.Body>

                        </Card>

                      
                        <Card >
                            <Card.Img variant="top" src={Tipo_entidad} />
                            <Card.Body>
                                <Card.Link href="/badges/tipo">TIPO ENTIDAD</Card.Link>
                            </Card.Body>
                        </Card>
                     

                        <Card >
                            <Card.Img variant="top" src={Red_navegable} />
                            <Card.Body>
                                <Card.Link href="/badges/navegable">RED NAVEGABLE</Card.Link>
                            </Card.Body>
                        </Card>

                        <Card >
                            <Card.Img variant="top" src={Potencial} />
                            <Card.Body>
                                <Card.Link href="/badges/potencial">POTENCIAL</Card.Link>
                            </Card.Body>

                            {/* <Link className="btn btn-primary" to="/badges">
                Start
              </Link> */}
                        </Card>
                        <Card style={{  visibility: 'hidden'}}>
                            
                            </Card>
                        <Card >
                            <Card.Img variant="top" src={Atributos} />
                            <Card.Body>
                                <Card.Link href="/badges/agua">ATRIBUTOS DE AGUA</Card.Link>
                            </Card.Body>
                        </Card>
                        <Card >
                            <Card.Img variant="top" src={Entidad} />
                            <Card.Body>
                                <Card.Link href="/badges/entidad">ENTIDAD</Card.Link>
                            </Card.Body>
                        </Card>

                    </CardColumns>

                </div>


                <div className="Badge__footer"></div>
            </div>

        )

    }
}

export default BadgeNew