import React from 'react';

import './styles/Badges.css';
//import confLogo from '../images/badge-header.svg';
import { Navbar } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import { InputGroup } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import api from '../api';
import BadgeTable from '../components/BadgeTable';
import LoadingOverlay from 'react-loading-overlay';

class Badge extends React.Component {

    state = {
        loading: true,
        error: null,
        data: undefined,

    };

    fetchData = async (param) => {
        this.setState({ loading: true, error: null });

        switch (param) {

            case 'dominio':

                try {
                    const data = await api.badges.listComplex();
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;
            case 'dominioSimple':

                try {
                    const data = await api.badges.listSimple();
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }

                break;
            case 'tipo':

                try {
                    const data = await api.badges.listTipo();
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;

            case 'navegable':

                try {
                    const data = await api.badges.listnavegable();
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;

            case 'potencial':

                try {
                    const data = await api.badges.listpotencial();
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;

            case 'agua':

                try {
                    const data = await api.badges.listagua();
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;


            case 'entidad':

                try {
                    const data = await api.badges.listentidad();
                    console.log(data)

                    this.setState({ loading: false, data: data });
                } catch (error) {
                    this.setState({ loading: true, error: error });
                }
                break;

                default:
                break;

        }


    };


    componentDidMount() {
        this.fetchData(this.props.match.params.domcom)
    }



    render() {

        return (

            <div  className="Badges__hero">






                {this.state.loading !== true ?
                    <div className="Badges__hero" >
                        <Navbar bg="light" expand="lg" style={{ justifyContent: 'space-between' }} >

                            <Navbar.Brand >NOMBRE DE LA ENTIDAD</Navbar.Brand>
                            <Button type="submit" href="/" variant="outline-secondary" >Regresar</Button>

                        </Navbar>

                        <Navbar bg="light" expand="lg">
                            <InputGroup className="mb-3">
                                <FormControl
                                    placeholder="Filtro"
                                    aria-label="Recipient's username"
                                    aria-describedby="basic-addon2"
                                />
                                <InputGroup.Append>
                                    <Button variant="outline-secondary">Nuevo</Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </Navbar>

                        <BadgeTable badges={this.state.data} param={this.props.match.params.domcom} />
                    </div>
                    :
                  
                <LoadingOverlay className="Badges__hero"
                active={true}
                spinner
                text='Cargando...'
            >
       
            </LoadingOverlay>

                }

                {this.state.error === true ? `Error: ${this.state.error.message}` : ''}

            </div>
        );
    }
}



export default Badge;
